%
%           Use this to load data files, verify settings,
%           and draw a Region of Interest (ROI) containing your
%           cell or whatever you are looking at.
%

UseFullImage = true; %Adjusts the image to occupy the full 'field of view'

% defaults to use when initializing the Processing Settings. You will be
% able to change and confirm these values when you run the script.
ProcSettings = struct;
ProcSettings.xCoordsColumn = '3';      % location of the x coordinates
ProcSettings.yCoordsColumn = '4';      % location of the y coordinates
ProcSettings.ChannelIDColumn = '0';    % location of the channel column (zero to ignore channel information)
ProcSettings.DataTableFileExt = 'csv'; % file extension of the the data tables
ProcSettings.DataDelimiter = 'comma';  % data-delimiter character. For whitespace characters use 'comma', 'tab', or 'space'.
ProcSettings.DataTableScale = '1';     % the scale of your coordinate data in nm (some like to save in 'camera pixels' which need to be converted first)
ProcSettings.ImageSize = '40960';      % size of the image field, in nanometres. Assumes square images.

%% Begin

ThisVersion = 1.0;

% housekeeping
home %clean up the command window
rng('shuffle')  % set the random seed
warning('off','MATLAB:mir_warning_maybe_uninitialized_temporary'); % disables a warning about a temporary variable in the parallel processing section. This is fine but warnings scare people so I am turning it off.
warning('off','MATLAB:MKDIR:DirectoryExists'); % don't warn about existing data folders

InfoMessage = ['---------------------Stage 5 - ROI Selection (v',num2str(ThisVersion),')---------------------'];
disp(InfoMessage);
clear InfoMessage ThisVersion

%% Get list of files

% Find the data files
    DataDirName = uigetdir(pwd,'Choose your data folder');
    
    if DataDirName ~= 0
        
        cd(DataDirName);

        % Ask about the data table
        prompt = {'xCoordsColumn', ...
                'yCoordsColumn', ...
                'ChannelIDColumn', ...
                'DataTableFileExt', ...
                'DataDelimiter', ...
                'DataTableScale', ...
                'ImageSize'};

        dlg_title = 'Describe your input data';
        num_lines = 1;
        defaults = {ProcSettings.xCoordsColumn, ...
                    ProcSettings.yCoordsColumn, ...
                    ProcSettings.ChannelIDColumn, ...
                    ProcSettings.DataTableFileExt, ...
                    ProcSettings.DataDelimiter, ...
                    ProcSettings.DataTableScale, ...
                    ProcSettings.ImageSize};

        answer = inputdlg(prompt,dlg_title,num_lines,defaults);

        if ~isempty(answer)
            ProcSettings.xCoordsColumn = str2double(answer{1,1});
            ProcSettings.yCoordsColumn = str2double(answer{2,1});
            ProcSettings.ChannelIDColumn = str2double(answer{3,1});
            ProcSettings.DataTableFileExt = answer{4,1};
            ProcSettings.DataDelimiter = answer{5,1};
            ProcSettings.DataTableScale = str2double(answer{6,1});
            ProcSettings.ImageSize = str2double(answer{7,1});
        else
            errordlg('Can''t proceed if you won''t answer my questions!');
            error('Can''t proceed if you won''t answer my questions!');
        end
        
        % get list of txt files
        DirFullList = dir;                              % Get the data for the current directory
        FolderIndex = [DirFullList.isdir];              % Find the index for directories
        FileList = {DirFullList(~FolderIndex).name}';   % Get a list of the files
        
        % Remove unwanted files from the list
        badFiles = [];                              
        for f = 1:length(FileList)
            [~, fname, ext] = fileparts(FileList{f,1});
            FileList{f,2} = fname;
            FileList{f,3} = ext;
            
            if ~strcmp(ext,['.',ProcSettings.DataTableFileExt])
                badFiles(end+1,1) = f;
            end
            if contains(fname,'ProcSettings') || contains(fname,'coords') || contains(fname,'notes')
                badFiles(end+1,1) = f;
            end
        end
        FileList(badFiles,:) = [];
        
        % Sort what remains
        [~, natsortidx] = sort_nat(FileList(:,2),'Ascend');
        FileList = FileList(natsortidx,:);
        
        if size(FileList, 1) == 0
            error('There are no files to analyse in the folder you selected!');
        end
        
    else
        
        error('Cancelled?! So rude.');
        
    end
       
    clear badFiles DirFullList ext f fname FolderIndex natsortidx
 
if size(FileList,1) > 0
% Display some information
    disp('---------------------------------------------------------');
    InfoMessage=[datestr(fix(clock),'HH:MM:SS'),9,'Preparing ' num2str(size(FileList,1)) ' data tables.'];
    disp(InfoMessage);
    disp('---------------------------------------------------------');
    clear InfoMessage

%% Set up a results folder
    
    % Check for an existing data folder
    if exist(fullfile(DataDirName,'SaveDataFolderName.mat'), 'file') ~= 0
    %     load(fullfile(DataDirName,'SaveDataFolderName.mat'));
        errordlg('Phase 1 folder already exists! You need to move to Phase 2.');
    else
        alphanums = ['a':'z' 'A':'Z' '0':'9'];
        randname = alphanums(randi(numel(alphanums),[1 5]));
        OutputFolderName = ['ROIs-(',randname,')'];
        mkdir(fullfile(DataDirName,OutputFolderName));
        cd(fullfile(DataDirName,OutputFolderName));
    end

    ProcSettings.SaveLowDPI = '-r600'; % for quicker saving of preview images

    % clean up
    clear alphanums randname

    for FileID = 1:size(FileList,1)
    
    % Load the file
        CurrentTableFileName=FileList{FileID,1};
        InfoMessage =  [datestr(fix(clock),'HH:MM:SS'),9,'[',num2str(FileID),'/',num2str(size(FileList,1)),'] ',CurrentTableFileName];
        disp(InfoMessage);
        clear InfoMessage

        % error if the data table file doesn't exist
        if exist(fullfile(DataDirName,CurrentTableFileName), 'file') == 0
            ErrorMessage = ['Problem! Cannot find the data table file ', CurrentTableFileName, '.'];
            errordlg(ErrorMessage);
            clear ErrorMessage
        end

        datatable = importdata(fullfile(DataDirName,CurrentTableFileName));

        % Check that we have a useable struct array with the data
        % if not make one from the data we did import.
        if ~isstruct(datatable) 
            data2 = datatable;
            datatable = struct;
            datatable.data = data2;
            
            datatable.colheaders = cell(1,size(data2,2));
            for i=1:size(data2,2)
                datatable.colheaders{i} = ['Data Col. ' num2str(i)];
            end
            clear i data2
        end
        
        % correct for any scaling, i.e. convert distances to real units (nm)
        if ProcSettings.DataTableScale ~= 1
            datatable.data(:,ProcSettings.xCoordsColumn) = datatable.data(:,ProcSettings.xCoordsColumn) * ProcSettings.DataTableScale;
            datatable.data(:,ProcSettings.yCoordsColumn) = datatable.data(:,ProcSettings.yCoordsColumn) * ProcSettings.DataTableScale;
        end
        
        % cut the number of points down to prevent crashes during plotting
        if size(datatable.data,1) > 500e3
            datatable_preview = datatable.data(1:500000,:);
        else
            datatable_preview = datatable.data;
        end
        
        % Determine the image boundaries from ProcSettings or try to guess
        % them from the imported table
        if isfield(ProcSettings,'ImageSize')
            ProcSettings.AxisLimits = [0 ProcSettings.ImageSize 0 ProcSettings.ImageSize];
        else
            % try to guess the limits from the image
            minX = floor(min(datatable.data(:,ProcSettings.xCoordsColumn))/1000)*1000;
            minY = floor(min(datatable.data(:,ProcSettings.yCoordsColumn))/1000)*1000;
            maxX = ceil(max(datatable.data(:,ProcSettings.xCoordsColumn))/1000)*1000;
            maxY = ceil(max(datatable.data(:,ProcSettings.yCoordsColumn))/1000)*1000;
            
            % assume images are square ...
            minXY = min(minX,minY);
            maxXY = max(maxX,maxY);
            ProcSettings.AxisLimits = [minXY maxXY minXY maxXY];

            clear minX minY maxX maxY minXY maxXY
        end
        
        ProcSettings.PlotWidth = ProcSettings.AxisLimits(2) - ProcSettings.AxisLimits(1);
        ProcSettings.SaveHighDPI = strcat('-r',num2str(ProcSettings.PlotWidth / 100)); % for 100 nm per pixel
                
        % set up some other stuff that we'll need for the current image table...
        ProcSettings.ExptTitle = FileList{FileID,2};

    %====== Plot the events
        % randomising the input rows makes matlab render faster for some
        % unknown reason. The rand idx here are kept for later plots.
        data_rand_ind = randperm(size(datatable_preview,1));                           % Create a vector of random integers from 1 to len
        data_randx = datatable_preview(data_rand_ind,ProcSettings.xCoordsColumn);  % Randomize the x input vectors.
        data_randy = datatable_preview(data_rand_ind,ProcSettings.yCoordsColumn);  % Randomize the y input vectors.

        [DataEvents_fig, DataEvents_axes] = DoMeAFigure(ProcSettings.AxisLimits,[0 0 0]);
              
        if ProcSettings.ChannelIDColumn ~= 0
            data_randz=datatable_preview(data_rand_ind,ProcSettings.ChannelIDColumn);
            EventsPlot = scatter3(data_randx,data_randy,datatable_preview(:,ProcSettings.ChannelIDColumn),1,'w.',data_randz);
            set(DataEvents_fig,'InvertHardcopy','off');
        else
            EventsPlot = scatter(data_randx,data_randy,1,'w.');
        end
        
        % make zoomed in axes
        if UseFullImage
            DataAxisLimits = ProcSettings.AxisLimits;
        else
            data_xmin = floor(min(datatable.data(:,ProcSettings.xCoordsColumn))/1000)*1000;
            data_xmax = ceil(max(datatable.data(:,ProcSettings.xCoordsColumn))/1000)*1000;
            data_ymin = floor(min(datatable.data(:,ProcSettings.yCoordsColumn))/1000)*1000;
            data_ymax = ceil(max(datatable.data(:,ProcSettings.yCoordsColumn))/1000)*1000;

            % fix to longest side
            aspect_ratio = (data_xmax-data_xmin) / (data_ymax-data_ymin);
            if aspect_ratio >= 1
                % go with x axis
                DataAxisLimits = [data_xmin data_xmax data_ymin (data_ymin + (data_xmax-data_xmin))];
                RoI_rescale = (data_xmax-data_xmin)/ProcSettings.AxisLimits(2);
            else
                % go with y axis
                DataAxisLimits = [data_xmin (data_xmin + (data_ymax-data_ymin)) data_ymin data_ymax];
            end
        end
        
        axis(DataAxisLimits)

    %====== Ask user for a ROI

        set(DataEvents_fig,'visible','on');
        set(DataEvents_fig,'units','normalized','position',[0 0 1 1]);
        set(EventsPlot,'CData',[1 0.02 0.2]);
        set(DataEvents_fig,'Color','k');

        NewRoI = true;
        ROIs_This_Image = 1;
        answer = inputdlg('ROIs to select:', 'ROIs to select (0 = skip this cell)', 1, {num2str(ROIs_This_Image)});

        if ~isempty(answer)
            ROIs_This_Image = str2double(answer{1,1});
        else
            errordlg('Need at least 1 ROI to proceed!');
            error('Need at least 1 ROI to proceed!');
        end

        if ROIs_This_Image > 0
            
            % make a temporary new set of axes to trick matlab in being faster while
            % drawing
            xl = get(gca,'xlim'); 
            yl = get(gca,'ylim'); 
            posn = get(gca,'pos'); 
            xc = get(gca,'xcolor'); 
            yc = get(gca,'ycolor'); 
            xsc = get(gca,'xscale'); 
            ysc = get(gca,'yscale'); 
            xdir = get(gca,'xdir'); 
            ydir = get(gca,'ydir');

            for roiID = 1:ROIs_This_Image

                ROI_unsaved = true;

                while ROI_unsaved
                    tempax = axes('pos',posn,...
                    'xlim',xl,'ylim',yl,...
                    'color','none',...
                    'xcolor',xc,'ycolor',yc,...
                    'xtick',[],'xticklabel','',...
                    'ytick',[],'yticklabel','',...
                    'xdir',xdir,'ydir',ydir,...
                    'xscale',xsc,'yscale',ysc); 
                    axis equal
                    axis(DataAxisLimits);

                    fcn = makeConstrainToRectFcn('imfreehand',get(gca,'XLim'),get(gca,'YLim'));
                    hFreeHand = imfreehand(gca,'Closed','true','PositionConstraintFcn',fcn);
                    roi_coords = hFreeHand.getPosition(); %get coords of imfreehand directly

                    % Construct a questdlg with options
                    CheckHappiness = questdlg(['ROI ', num2str(roiID),' of ', num2str(ROIs_This_Image),': Are you happy with this ROI?'], ...
                        'Happiness Check', ...
                        'Save ROI and continue','Delete ROI and redraw','Save ROI and continue');
                    % Handle response
                    switch CheckHappiness
                        case 'Save ROI and continue'
                            disp(['Saving ROI', num2str(roiID),'...'])
                            delete(tempax) % Delete temporary set of axes

                            roi_coords(end+1,:) = roi_coords(1,:); % repeat first coord to close the ROI shape
                            roi_json = jsonencode(roi_coords);

                            %save ROI
                            json_roi_fname = [FileList{FileID,2}, '_ROI_', num2str(roiID), '.json'];

                            fid = fopen(fullfile(pwd, json_roi_fname),'w');
                            fprintf(fid,'%s',roi_json);
                            fclose(fid);

                            ROI_unsaved = false; % we have saved the ROI, exit the while-loop

                        case 'Delete ROI and redraw'
                            delete(tempax)
                            clear fcn hFH pos
                    end

                end % end of ROI-suitability checking

                plot(roi_coords(:,1), roi_coords(:,2), 'LineWidth',2, 'Color','w') % plot the ROI
                text(mean(roi_coords(:,1)),mean(roi_coords(:,2)), num2str(roiID), 'color','white','FontSize',20) % label it
                clear cell_bounds pos

            end % end of multi-ROIs for this image
            fnsave = [FileList{FileID,2}, '_AllROIs.png'];
            print(DataEvents_fig,'-dpng',ProcSettings.SaveHighDPI,fullfile(pwd, fnsave));
            close(DataEvents_fig)
            clear DataEvents_fig DataEvents_axes tempax xl yl xc yc xsc ysc xdir ydir hFH fnsave fcn EventsPlot
        
        else
            
            % zero ROIs entered, skip this cell
            close(DataEvents_fig)
            clear DataEvents_fig DataEvents_axes EventsPlot
        
        end % end of this image
        
    end % end of file-list
    
    disp('Finished!');
    disp(['Your ROIs are saved to:', fullfile(DataDirName,OutputFolderName)]);

else
    disp('---------------------------------------------------------');
    disp(['No files with extension ''.', ProcSettings.DataTableFileExt, ''' were found! Nothing to do :)']);
    disp('---------------------------------------------------------');
end
