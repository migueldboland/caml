# CAML

Cluster Analysis by Machine Learning (CAML) for Single Molecule Localization Microscopy (SMLM) data.

Please check the [CAML Manual page](https://gitlab.com/quokka79/caml/wikis/home).